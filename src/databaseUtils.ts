import PouchDB from "pouchdb";
import { existsSync, mkdirSync } from "fs";
import faker from "faker";
import Account from "./models/account";
import PayeeModel from "./models/payee";
import Transaction from "./models/transaction";

export function createDatabase({
  dbName,
  dbPath = "",
}: {
  dbName: string;
  dbPath: string;
}): PouchDB.Database {
  // create new directoryj
  try {
    // first check if directory already exists
    if (dbPath && !existsSync(dbPath)) {
      mkdirSync(dbPath);
      console.log("Directory is created.");
    } else {
      console.log("Directory already exists.");
    }
  } catch (err) {
    console.log(err);
  }
  return new PouchDB(`${dbPath}/${dbName}`);
}

export async function refreshIndices({ db }: { db: PouchDB.Database }) {
  return Promise.all([
    await db.createIndex({
      index: {
        fields: ["table"],
      },
    }),

    await db.createIndex({
      index: {
        fields: ["createdAt"],
      },
    }),
  ]);
}

export async function populateDatabase({
  db,
  count = 0,
}: {
  db: PouchDB.Database;
  count: number;
}) {
  // Insert X randomly generated Account records.
  const items = [];
  for (let c = 0; c < count; c++) {
    items.push(
      Account.create(db, {
        name: faker.finance.accountName(),
        type: faker.random.arrayElement([
          Account.Types.Checking,
          Account.Types.CreditCard,
          Account.Types.Saving,
        ]),
        createdAt: faker.date.past(50),
        modifiedAt: faker.date.past(50),
      })
    );
  }
  const savedAccounts = await Promise.all(items);

  let payeePromises = [];
  for (let p = 0; p < 2; p++) {
    payeePromises.push(
      PayeeModel.create(db, {
        name: faker.company.companyName(),
      })
    );
  }
  const payees = await Promise.all(payeePromises);

  // Insert X number of randomly generate Transaction records.
  savedAccounts.map((account) => {
    Transaction.create({
      db,
      input: {
        name: faker.finance.transactionDescription(),
        amount: parseFloat(faker.finance.amount()),
        type: faker.random.arrayElement([
          Transaction.Types.Debit,
          Transaction.Types.Credit,
        ]),
        payeeId: faker.random.arrayElement(payees).id,
        accountId: account.account.id,
      },
    });
  });
}

export async function resetDatabase({
  db,
  dbName,
  dbPath,
}: {
  db: PouchDB.Database;
  dbName: string;
  dbPath: string;
}) {
  await db.destroy();
  return createDatabase({ dbName, dbPath });
}

export async function replicateDatabase({
  db,
  remoteUrl,
  username,
  password,
}: {
  db: PouchDB.Database;
  remoteUrl: string;
  username: string;
  password: string;
}) {
  return (
    db.replicate
      // .to("http://admin:admin@127.0.0.1:5984/budglet")
      // TODO: There has to be a better way to pass this info to the DB...
      .to(`http://${username}:${password}@${remoteUrl}`)
      .on("complete", () => console.log("Replication complete!"))
      .on("error", (error) => console.log("Replication error!", error))
  );
}
