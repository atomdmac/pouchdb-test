import PouchDB from "pouchdb";
import PouchDBFind from "pouchdb-find";
import {
  createDatabase,
  populateDatabase,
  refreshIndices,
  replicateDatabase,
  resetDatabase,
} from "./databaseUtils";
import Account from "./models/account";
import Transaction from "./models/transaction";

// Initialize the database.
PouchDB.plugin(PouchDBFind);

const dbName = "budglet";
const dbPath = "./databases";
const remoteUrl = `127.0.0.1:5984/${dbName}`;
const username = "admin";
const password = "admin";

/**
 * Parse CLI arguments and convert "--long" flags to key/value pairs
 */
function getArguments() {
  function toKeyValue(output: { [key: string]: unknown }, arg: string) {
    const pair = arg.split("=");
    output[pair[0].split("--")[1]] = pair[1] || true;
    return output;
  }
  return process.argv.slice(2).reduce(toKeyValue, {});
}

async function main() {
  const args = getArguments();
  let db = createDatabase({ dbName, dbPath });

  if (args["reset"] === true) {
    console.log("Reseting the database...");
    db = await resetDatabase({ db, dbName, dbPath });
    // Force refresh of indices
    args["refresh"] = true;
  }

  if (args["populate"]) {
    console.log(`Populating DB with ${args["populate"]} items`);
    await populateDatabase({ db, count: args["populate"] as number });
  }

  if (args["refresh"]) {
    console.log("Refreshing all indices...");
    await refreshIndices({ db });
  }

  if (args["replicate"]) {
    console.log(`Replicating DB to ${remoteUrl}`);
    await replicateDatabase({ db, remoteUrl, username, password });
  }

  console.log("Getting database contents...");
  const { rows } = await db.allDocs({ include_docs: true });
  console.log(`Database currently contains ${rows.length} items.`);

  if (args["list"]) {
    rows.forEach((r) => console.log(r));
    return;
  }

  const account = await db.find({
    fields: ["_id", "name"],
    selector: {
      docType: Account.docType,
      // createdAt: {
      // $gt: "1985-05-22T02:41:50.290Z",
      // },
    },
    // sort: [{ createdAt: "desc" }],
    limit: 5,
  });

  console.log("Accounts: ", account);

  const results = await db.find({
    fields: [
      "_id",
      "docType",
      "type",
      "amount",
      "createdAt",
      "accountId",
      "payeeId",
    ],
    selector: {
      docType: Transaction.docType,
      accountId: {
        $in: account.docs.map((a) => a._id),
      },
    },
  });

  console.log("Search Results: ", results);
}

main();
