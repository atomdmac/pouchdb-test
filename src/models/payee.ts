import PouchDB from "pouchdb";
import ModelBase from "./base";

namespace Payee {
  export const docType = "payee";

  export enum SpecialPayees {
    StartingBalance = "StartingBalance",
  }

  export type CreateInput = {
    name: string;
  } & Partial<ModelBase.Timestamps>;

  export type Doc = ModelBase.Doc<CreateInput>;

  export async function create(db: PouchDB.Database, payee: Payee.CreateInput) {
    return await ModelBase.create<CreateInput>(db, payee, docType);
  }

  export async function exists(db: PouchDB.Database, id: string) {
    if (id in SpecialPayees) {
      return true;
    }
    return await ModelBase.exists(db, id, docType);
  }
}

export default Payee;
