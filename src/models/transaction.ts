import PouchDB from "pouchdb";
import ModelBase from "./base";
import Account from "./account";
import Payee from "./payee";

namespace Transaction {
  export const docType = "transaction";
  export enum Types {
    Debit = "Debit",
    Credit = "Credit",
    StartingBalance = "Starting Balance",
  }
  export type CreateInput = {
    name: string;
    amount: number;
    type: Types;
    payeeId: string;
    accountId: string;
  } & Partial<ModelBase.Timestamps>;

  export type Doc = ModelBase.Doc<CreateInput>;

  export async function create({
    db,
    input,
  }: {
    db: PouchDB.Database;
    input: Transaction.CreateInput;
  }) {
    if (!(await Account.exists(db, input.accountId))) {
      throw new Error(
        "Could not create transaction because account was not found."
      );
    }

    if (!(await Payee.exists(db, input.payeeId))) {
      throw new Error(
        "Could not create transaction because payee was not found."
      );
    }

    return await ModelBase.create<Transaction.CreateInput>(db, input, docType);
  }

  export async function exists(db: PouchDB.Database, id: string) {
    return await ModelBase.exists(db, id, docType);
  }
}

export default Transaction;
