import { v4 as uuid } from "uuid";
import AccountModel from "./account";
import PayeeModel from "./payee";
import TransactionModel from "./transaction";

namespace ModelBase {
  export type Timestamps = {
    createdAt: Date;
    modifiedAt: Date;
  };

  export type DocType =
    | typeof AccountModel.docType
    | typeof PayeeModel.docType
    | typeof TransactionModel.docType;

  export type GenericDoc = {
    _id: string;
    docType: DocType;
  };

  export type Doc<T> = GenericDoc & Timestamps & T;

  export type Update<T> = Partial<Omit<Doc<T>, "id" | "docType">>;

  export type Query<T> = Partial<Doc<T>> & Partial<T>;

  export type Response = {
    id: string;
    ok: boolean;
    rev: string;
  };

  export async function getNextId({ docType }: { docType?: string }) {
    return `${docType || ""}/${uuid()}`;
  }

  export async function create<T>(
    db: PouchDB.Database,
    input: T,
    docType: DocType
  ): Promise<Response> {
    return await db.put<T>({
      _id: await ModelBase.getNextId({ docType }),
      createdAt: new Date(),
      modifiedAt: new Date(),
      docType,
      ...input,
    });
  }

  export async function exists(
    db: PouchDB.Database,
    id: string,
    docType: DocType
  ) {
    const existing = await db.find({
      selector: {
        _id: id,
        docType,
      },
    });

    return !!existing.docs.length;
  }
}

export default ModelBase;
