import PouchDB from "pouchdb";
import ModelBase from "./base";
import Transaction from "./transaction";
import Payee from "./payee";

namespace Account {
  export const docType = "account";
  export enum Types {
    Checking = "Checking",
    Saving = "Saving",
    CreditCard = "CreditCard",
  }

  export type CreateInput = {
    name: string;
    type: Types;
  } & Partial<ModelBase.Timestamps>;

  export type Doc = ModelBase.Doc<CreateInput>;

  export async function create(
    db: PouchDB.Database,
    account: Account.CreateInput,
    startingBalance: number = 0
  ) {
    const accountRecord = await ModelBase.create<Account.CreateInput>(
      db,
      account,
      docType
    );

    const startingBalanceRecord = await Transaction.create({
      db,
      input: {
        name: "Starting Balance",
        amount: startingBalance,
        type: Transaction.Types.StartingBalance,
        payeeId: Payee.SpecialPayees.StartingBalance,
        accountId: accountRecord.id,
      },
    });
    return {
      account: accountRecord,
      startingBalance: startingBalanceRecord,
    };
  }

  export async function exists(db: PouchDB.Database, id: string) {
    return await ModelBase.exists(db, id, docType);
  }
}

export default Account;
